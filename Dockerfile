FROM alpine

COPY bin/ /usr/bin/

RUN apk add curl certbot && \
    curl -sL https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl -o /usr/bin/kubectl && \
   chmod +x /usr/bin/kubectl

CMD /bin/sh
