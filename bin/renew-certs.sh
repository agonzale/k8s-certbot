#!/bin/sh -x
#
# Simple simple simple
#

CERTIFICATE_FQDN=$1

if [ -z "$CERTIFICATE_FQDN" ];
then
    echo "Use: $0 <CERTIFICATE_FQDN>" >&2
    exit 1
fi

if [ -z "$EMAIL_CERT" ];
then
    EMAIL_CERT="it-service-its@cern.ch"
fi

echo "CERTIFICATE FQDN: $CERTIFICATE_FQDN."; echo

CERTF="/etc/letsencrypt/live/$CERTIFICATE_FQDN/cert.pem"
#CACERTF="/etc/letsencrypt/live/$CERTIFICATE_FQDN/chain.pem"
KEYF="/etc/letsencrypt/live/$CERTIFICATE_FQDN/privkey.pem"

# If the certificate is not yet there, make a request
if [ ! -f "$CERTF" ];
then
    echo "No certificate found";

    certbot certonly \
        --config-dir /etc/letsencrypt/ \
        --work-dir /etc/letsencrypt/ \
        --logs-dir /etc/letsencrypt/ \
        --webroot \
        --webroot-path /etc/letsencrypt/ \
        --agree-tos \
        -m "$EMAIL_CERT" \
        -d "$CERTIFICATE_FQDN" \
        -n

    kubectl create secret tls tls-crt --cert='$CERTF' --key='$KEYF'
    #
    exit 0
fi

echo "Renewing certificate."; echo

certbot renew \
    --config-dir /etc/letsencrypt/ \
    --work-dir /etc/letsencrypt/ \
    --logs-dir /etc/letsencrypt/ \
    --webroot-path /etc/letsencrypt/html

 if [ $? -ne 0 ];
then
    echo "#"
    echo "# ERROR: Cannot renew the CERTIFICATE_FQDN"
    echo "#"
    exit 3
fi

kubectl patch secret tls-crt -p "{\"data\":{\"tls.crt\":\"$(cat $CERTF | base64 | tr -d '\n')\",\"tls.key\":\"$(cat $KEYF | base64 | tr -d '\n')\"}}"

